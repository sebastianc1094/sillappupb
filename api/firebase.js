import * as firebase from 'firebase'

var config = {
    apiKey: "AIzaSyA4nlIC9JRnJXpiZVA4tUcdua0ZOzmcpeM",
    authDomain: "sillappupb.firebaseapp.com",
    databaseURL: "https://sillappupb.firebaseio.com",
    projectId: "sillappupb",
    storageBucket: "sillappupb.appspot.com",
    messagingSenderId: "892155066793"
};

const firebaseAPI = firebase.initializeApp(config);

export const firebaseAuth = firebase.auth();
export const firebaseDatabase = firebase.database();
export const facebookProvider = firebase.auth.FacebookAuthProvider;

export default firebaseAPI