import React from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView, Image, TouchableOpacity, Dimensions, ImageBackground } from 'react-native';
import { Form, Icon, Input, Item, Container, Content } from 'native-base';
import { FormLabel, FormInput, Button, SocialIcon } from 'react-native-elements';
import Expo from 'expo';

const { width, height } = Dimensions.get("window");

import {firebaseAPI, firebaseAuth, facebookProvider } from '../../api/firebase';

export default class LoginScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
        }
    }
    
    handleSignIn(){
        const { email, password } = this.state;
        if(email.length < 4){
            alert('Por favor ingrese un correo válido');
            return;
        }
        if(password.length < 4){
            alert('Por favor ingrese una contraseña de más de 4 caracteres');
            return;
        }
        firebaseAuth.signInWithEmailAndPassword(email,password)
            .then(() => { this.props.navigation.navigate('HomeScreen') })
            .catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;

            if(errorCode == 'auth/wrong-password'){
                alert('Contraseña errónea');
            }
            if (errorCode == 'auth/user-not-found'){
                alert('Correo erróneo');
            }
        });
    }

    loginFacebook = async () => {
        const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
            '3337302896338680', { permissions: ['public_profile'] });

        if (type == 'success') {
            const credential = facebookProvider.credential(token);
            console.log(credential);
           
            firebaseAuth.signInWithCredential(credential)
                .then(() => { this.props.navigation.navigate('HomeScreen') })
                .catch((error) => {
                    console.log(error)
            });

        }else{
            alert(type);
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>     
                <ImageBackground source={require('../../img/login.png')} style={{ width , height }} resizeMode="cover">        
                <View style = {{ alignItems: 'center', justifyContent:'center', flexGrow: 0.5 }}>
                    <Image style = {{ height: 110, width: 110}} source={require('../../img/logo.png')}/>
                </View>
                <View style={{ marginTop:10 }}>                               
                    <FormLabel labelStyle={{ color: 'black', fontSize: 14 }}>Correo electrónico</FormLabel>
                        <FormInput inputStyle={{ color: 'black', fontSize: 15 }} value={this.state.email} onChangeText={email => this.setState({ email })} placeholder ="usuario@upb.edu.co"/>                  
                    <FormLabel labelStyle={{ color: 'black', fontSize: 14 }}>Contraseña</FormLabel>
                        <FormInput inputStyle={{ color: 'black', fontSize: 15 }} onChangeText={password => this.setState({ password })} secureTextEntry/>               
                </View>
                <View style={{ margin: 15 , marginTop: 20 }}>
                    <View>
                        <SocialIcon raised title='Iniciar sesión' button style={{ backgroundColor: '#4A148C' }} onPress={this.handleSignIn.bind(this)}/>
                    </View>
                    <View style={{ margin: 20, marginTop: 10 }}>
                            <SocialIcon onPress={() => this.loginFacebook()} title='Iniciar con Facebook' button type='facebook' />
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop:20 }}>
                        <Text style={{ color: 'black', fontSize: 15 }}>¿No tiene una cuenta?</Text>
                            <TouchableOpacity activeOpacity={.5} onPress={() => navigate('RegisterScreen')}>
                                <Text style = {{fontSize: 18}}>Registrarse</Text>
                            </TouchableOpacity>                       
                    </View>                     
                </View>    
                </ImageBackground>                                                            
            </KeyboardAvoidingView>            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
    },
});
