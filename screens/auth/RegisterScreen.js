import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { FormLabel, FormInput, Button, SocialIcon, Icon } from 'react-native-elements';

const { width, height } = Dimensions.get("window");

import firebaseAPI from '../../api/firebase';

export default class RegisterScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password: '',
            passwordValidate: ''
        }
    }

    handleSignUp() {
        const { email, password, passwordValidate } = this.state;
        if(email.length < 4){
            alert('Por favor ingrese un correo válido');
            return;
        }
        if(password.length < 4){
            alert('Por favor ingrese una contraseña de más de 4 caracteres');
            return;
        }
        if(password === passwordValidate){
            firebaseAPI.auth().createUserWithEmailAndPassword(email,password).catch(function(error){
                var errorCode = error.code;
                var errorMessage = error.message;

                if(errorCode == 'auth/weak-password'){
                    alert('La contraseña es débil');
                } else {
                    alert(errorMessage);
                }
            }).then(() => { alert('Cuenta creada') })
            .then(() => { this.props.navigation.navigate('LoginScreen') });
        } else {
            this.setState({
                password:'',
                passwordValidate:''
            });
            alert('Las contraseñas no coinciden');    
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        const { goBack } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../img/regbg.png')} style={{ width, height }} resizeMode="cover">
                    <View style={{ alignItems:"flex-start", margin:20}}>
                        <TouchableOpacity onPress={() => goBack()}>
                            <Icon name='arrow-back' color='white'/>
                        </TouchableOpacity>  
                    </View> 
                    <View style={{ marginTop: 45 }}>
                        <FormLabel labelStyle={{ color: 'black', fontSize: 15 }}>Correo electrónico</FormLabel>
                        <FormInput inputStyle={{ color: 'black', fontSize: 15 }} value={this.state.email} onChangeText={email => this.setState({ email })} placeholder="usuario@upb.edu.co" />
                        <FormLabel labelStyle={{ color: 'black', fontSize: 15 }}>Contraseña</FormLabel>
                        <FormInput secureTextEntry value={this.state.password} onChangeText={password => this.setState({ password })} />
                        <FormLabel labelStyle={{ color: 'black', fontSize: 15 }}>Confirmar contraseña</FormLabel>
                        <FormInput secureTextEntry value={this.state.passwordValidate} onChangeText={passwordValidate => this.setState({ passwordValidate })} />
                    </View>   
                    <View style={{ margin: 15, marginTop: 20 }}>
                        <View>
                            <SocialIcon raised title='Registrarme' button style={{ backgroundColor: '#4A148C' }} onPress={this.handleSignUp.bind(this)} />
                        </View>
                    </View>    
                </ImageBackground>     
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
