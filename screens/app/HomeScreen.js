import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Icon, Button } from 'react-native-elements'
import { firebaseAuth, firebaseAPI } from '../../api/firebase';

const { width, height } = Dimensions.get("window");

export default class HomeScreen extends React.Component {
    logout() {
        firebaseAuth.signOut().then(() => {
            this.props.navigation.navigate('LoginScreen');
        });
    }

    render() {
        const { navigate } = this.props.navigation;
        var user = firebaseAuth.currentUser;
        if(user != null){
            var email_id = user.email;
        }
        return (
            <ImageBackground source={require('../../img/home.png')} style={{ width, height }} resizeMode="cover">
            <View style={styles.container}>
                <View style={[styles.menu]}>
                    <View style={styles.title}>
                        <Text style={styles.textStyle}>Selecciona el color de la zona donde te quieres sentar:</Text>
                    </View>
                    <View style={styles.btnsSup}>
                        <Button buttonStyle={{
                            backgroundColor: "blue",
                            borderColor: "transparent",
                            width: 80,
                            height: 80,
                            borderWidth: 0,
                            borderRadius: 40
                        }} onPress={() => navigate('TableScreen', { colortablex: 'blue'})}/>
                        <Button buttonStyle={{
                            backgroundColor: "red",
                            borderColor: "transparent",
                            width: 80,
                            height: 80,
                            borderWidth: 0,
                            borderRadius: 40
                        }} onPress={() => navigate('TableScreen', { colortablex: 'red' })}/>
                        <Button buttonStyle={{
                            backgroundColor: "green",
                            borderColor: "transparent",
                            width: 80,
                            height: 80,
                            borderWidth: 0,
                            borderRadius: 40
                        }} onPress={() => navigate('TableScreen', { colortablex: 'green' })}/>
                    </View>
                    <View style={styles.btnsInf}>
                        <Button buttonStyle={{
                            backgroundColor: "orange",
                            borderColor: "transparent",
                            width: 80,
                            height: 80,
                            borderWidth: 0,
                            borderRadius: 40
                        }} onPress={() => navigate('TableScreen', { colortablex: 'orange' })}/>
                        <Button buttonStyle={{
                            backgroundColor: "yellow",
                            borderColor: "transparent",
                            width: 80,
                            height: 80,
                            borderWidth: 0,
                            borderRadius: 40
                        }} onPress={() => navigate('TableScreen', { colortablex: 'yellow' })}/>
                        <Button buttonStyle={{
                            backgroundColor: "cyan",
                            borderColor: "transparent",
                            width: 80,
                            height: 80,
                            borderWidth: 0,
                            borderRadius: 40
                        }} onPress={() => navigate('TableScreen', { colortablex: 'cyan' })}/>
                    </View>
                </View>
                
                <View style={[styles.box, styles.usr]}>
                    <View style ={styles.iconContainer}>
                        <Icon name='account-circle' iconStyle={{ fontSize: 35, color: 'black'}}/>                                             
                    </View>
                    <View style={styles.textContainer}>
                        <Text>Usuario activo: { email_id } </Text>
                    </View>   
                    <View style={styles.crossContainer}>
                        <Icon name='clear' iconStyle={{ fontSize: 30, color: 'white' }} onPress={() => this.logout()} />
                    </View>                 
                </View>
            </View>
            </ImageBackground> 
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    box: {
        height: 50 //set this one
    },
    menu: {
        flex:20,
        
    },
    usr: {
        flexDirection: 'row',
        backgroundColor: '#E0E0E0',
    },
    iconContainer: {
        flex: 1,
        width: 50,
        height: 50,
        justifyContent: 'center',
    },
    textContainer: {
        flex: 5,
        justifyContent: 'center',
    },
    crossContainer: {
        flex: 1,
        width: 50,
        height: 50,
        backgroundColor: 'red',
        justifyContent: 'center',
    },
    btnsSup: {
        marginTop: 80,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnsInf: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 120
    },
    textStyle: {
        fontSize: 18,
        textAlign: 'center',
        justifyContent: 'center'
    }
});
