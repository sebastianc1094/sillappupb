import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import TablePanel from '../../components/TablePanel';

export default class TableScreen extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        let { params } = this.props.navigation.state;
        return (
            <View style={styles.container}>
                <TablePanel properties = { params }/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1A237E',
    },
});
