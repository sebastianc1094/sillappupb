import React from 'react';
import { StyleSheet, Text, View, StatusBar, YellowBox } from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';

//Importación de las pantallas de autenticación
import LoginScreen from './screens/auth/LoginScreen';
import RegisterScreen from './screens/auth/RegisterScreen';

//Importación de las pantallas de la aplicación
import HomeScreen from './screens/app/HomeScreen';
import TableScreen from './screens/app/TableScreen';

export default class App extends React.Component {
  constructor(){
    super();
    console.ignoredYellowBox = ['Setting a timer'];
  }


  componentWillMount() {
    StatusBar.setHidden(true);
  }

  render() {
    return (
      <AuthStack />
    );
  }
}

const AppStack = StackNavigator({
  HomeScreen: { screen: HomeScreen,
    navigationOptions:{ header:null }},
  TableScreen: { screen: TableScreen,
    navigationOptions:{ header:null }},
});

const AuthStack = StackNavigator({
  LoginScreen: { screen: LoginScreen,
  navigationOptions: { header: null }},
  RegisterScreen: { screen: RegisterScreen,
    navigationOptions: { header: null }},
  AppStack: { screen: AppStack,
  navigationOptions: { header:null }},
});

const blockHome = AppStack.router.getStateForAction;
AppStack.router.getStateForAction = (action, state) => {
  if (state && action.type === NavigationActions.BACK && state.routes[state.index].routeName === "HomeScreen") {
    return null;
  }
  return blockHome(action, state);
};

const blockAuth = AuthStack.router.getStateForAction;
AuthStack.router.getStateForAction = (action, state) => {
  if (state && action.type === NavigationActions.BACK && state.routes[state.index].routeName === "LoginScreen") {
    return null
  }
  return blockAuth(action, state);
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
