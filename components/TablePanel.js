import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, ListView, ImageBackground, Dimensions } from 'react-native';

import Table from './Table';
const { width, height } = Dimensions.get("window");

export default class TablePanel extends React.Component {
    constructor(props) {
        super(props);
        const colorstatus = props.properties.colortablex + "";
        const data = Array(12).fill(Table).map(table => {
            return <Table referencia={colorstatus} />
        });

        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows(data),
        }
    }

    _renderRow(data) {
        return (
            <View style={styles.box}>
                {data}
            </View>
        );
    }

    jewelStyle = function(myColor) {
        return {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: myColor,
        }
    }

    render() {
        let color = this.props.properties.colortablex + "";
        return (           
            <View style={this.jewelStyle(color)}>
                <ImageBackground source={require('../img/res2.png')} style={{ width, height }} resizeMode="cover">
                <ListView renderRow={this._renderRow.bind(this)}
                    dataSource={this.state.dataSource}
                    contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                    pageSize={this.data} />
                </ImageBackground>
            </View>        
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    panel: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 15,
        backgroundColor: 'transparent'
    }
});
