import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import store from "./Globals";
import { firebaseDatabase, firebaseAuth } from '../api/firebase';

export default class Table extends React.Component {
    state = {
        use: false,
    }

    handlePress = () => {

        if (store.lockedChair || store.lockedChair == null) { // cuando no estoy sentando en ninguna debo bloquear alguna , null es cuando va hacer la primera vez y no he bloqueado ninguna
            store.lockedChair = false;
            this.setState({ use: !this.state.use });
            this.toggleTouch();
        } else if (this.state.use) { // cuando estoy sentando ya en alguna la puedo desbloquear
            store.lockedChair = true;
            this.setState({ use: !this.state.use });
            this.toggleTouch();
        } else { // cuando ya estoy sentando no puedo usar mas !
            console.log("ESTADO", "BLOQUEADO !" + store.lockedChair);
        }
    }

    getTableRef = () => {
        return firebaseDatabase.ref(this.props.referencia + "");
    }

    toggleTouch = (sit) => {
        const { uid } = firebaseAuth.currentUser;
        this.getTableRef().transaction(function (table) {
            if (table) {
                if (table.sit && table.sit[uid]) {
                    table.sit[uid] = null;
                } else {
                    if (!table.sit) {
                        table.sit = {};
                    }
                    table.sit[uid] = true;
                }
            }
            return table || {
                sit: {
                    [uid]: true,
                }
            };
        });
    }

    render() {
        const active = firebaseAuth.currentUser;
        if (active != null) {
            var email_id = active.email;
        }

        const useIcon = this.state.use ?
            <Icon name="ios-man" size={50} color='black' /> :
            <Icon name="ios-pin" size={50} color='gray' />

        const user = this.state.use ?
            <Text style={{ fontSize: 5 }}>{email_id}</Text> :
            <Text>Libre</Text>

        return (
            
            <View style={styles.container}>
                
                <TouchableOpacity onPress={this.handlePress}>
                    {useIcon}
                    <Image
                        source={require('../img/chair.png')}
                        style={styles.table}
                    />
                    
                </TouchableOpacity>
                {user}
            </View>
             
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        margin: 20,
        marginTop: 100,
    },
    table: {
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
